import os
import logging
import json

import requests

from api.clients import toshokan
from framework.json_encoder import ToshokanAPIJSONEncoder

update_filter = [
    {
        "$project": {
            "resume": "$_id",
            "time": "$clusterTime",
            "user":"$fullDocument.user",
            "ns": "list",
            "operation": "$operationType",
            "library_ref": "$fullDocument.library_ref",
            "fields": {
                "$ifNull":["$updateDescription.updatedFields","$fullDocument"]
                },
            "entry_id": "$documentKey._id"
            }
        },
    {
        "$project":{
            "fields._id":0,
            "fields.library_ref":0,
            "fields.user":0,
            "fields.last_modified":0,
            "fields.date_added":0
            }
        }
]
def watch_changes():
    s = requests.Session()

    logging.info("Watching changes...")
    last = [ev for ev in toshokan.events.find({},{"resume":1}).sort("_id", -1).limit(1)]
    resume = last[0]["resume"] if len(last) and last[0].get("resume") else None
    
    if os.environ.get("RESUME_FROM") != "false":
        resume = os.environ.get("RESUME_FROM")
    if resume == "None":
        resume = None

    cursor = toshokan.lists.watch(update_filter, full_document="updateLookup", resume_after=resume)
    for change in cursor:
        del change["_id"] # removed separately because pymongo needs it to be in the change event
        if not change.get("fields"):
            continue
        toshokan.events.insert_one(change)
        del change["resume"]
        del change["entry_id"]
        s.post(os.environ.get("NCHAN_PUB"), params={"id":change["user"]}, headers={'Content-Type': 'application/json'}, data=json.dumps(change, cls=ToshokanAPIJSONEncoder))
        logging.debug("Capture ", change["time"])

watch_changes()
