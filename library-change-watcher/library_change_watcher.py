import logging
import json
import os

import requests
import pymongo
from api.clients import toshokan
from framework.json_encoder import ToshokanAPIJSONEncoder

update_filter = [
    {
        "$match": { "$or": [
            {"ns.coll": {"$in": ["anime", "anime_staff", "anime_cast", "anime_episode", "manga", "manga_cast", "novel", "novel_cast", "character", "person", "vn", "vn_producer", "vn_character", "vn_person", "vn_release", "vn_tag", "vn_trait", "vn_relations", "vn_anime_relations", "vn_staff", "vn_cast", "vn_production"] } },
            {"operationType": "invalidate"} # need to catch all invalidate events in the db to resume after.
            ]}
        
    },
    {
        "$project": {
            "resume": "$_id",
            "time": "$clusterTime",
            "class": "$ns.coll",
            "operation": "$operationType",
            "entry_id": "$documentKey._id",
            "fields": {
                "$ifNull":["$updateDescription.updatedFields","$fullDocument"]
                },
            "removed_fields": "$updateDescription.removedFields"
            }
        },
    {
        "$project":{
            "fields.last_modified":0,
            }
        }
]
def iterate_cursor(resume):
    cursor = toshokan.watch(update_filter, resume_after=resume)
    for change in cursor:
        try:
            del change["_id"]
            # ignore empty update events
            if change.get("operation") == "update" and (not change.get("fields") and not change.get("removed_fields")):
                continue
            toshokan.library_log.insert_one(change)
            del change["resume"]
            s.post(os.environ.get("NCHAN_PUB"), params={"id":"library"}, headers={'Content-Type': 'application/json'}, data=json.dumps(change, cls=ToshokanAPIJSONEncoder))
        except:
            continue

    logging.info("Resuming after invalidate event.")
    iterate_cursor(change["resume"])

def watch_changes():
    logging.info("Watching library changes...")
    last = [ev for ev in toshokan.library_log.find({},{"resume":1}).sort("time", -1).limit(1)]
    resume = last[0]["resume"] if len(last) and last[0].get("resume") else None
    if os.environ.get("RESUME_FROM") != "false":
        resume = os.environ.get("RESUME_FROM")
    if resume == "None":
        resume = None
        logging.info("Resuming from last event.")
    try:
        iterate_cursor(resume)
    except pymongo.errors.OperationFailure:
        iterate_cursor(None)

watch_changes()